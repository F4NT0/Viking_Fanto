* Manual de uso do Programa Vikings

**Criando um programa no Viking**

1) Escreva um programa com a linguagem Assembly na Tela Program
2) Vá na área Program acima e selecione Save as
3) Selecione onde deseja que o programa fique salvo

**Compilando um programa no Viking**

1) Faça um Load ou escreva um programa na Tela  Program
2) Quando tiver escrito o programa, selecione o botão Compilar
3) Irá aparecer ao lado os espaços da memoria onde cada informação armazena
4) Deve aparecer uma mensagem dizendo que foi concluido a Compilação

**Iniciando um programa no Viking**

1) Depois de compilar o programa
2) Selecione o botão Iniciar
3) O programa irá começar a trabalhar

**Reiniciando um programa no Viking**

1) Depois que um programa termina sua função
2) Para reiniciar o programa, clique no botão reiniciar


**Passo-a-Passo da resolução do programa**

1) Resete o programa ou inicie ele com o botão Passo-A-Passo
2) Deve clicar no botão para mostrar cada passo-a-passo
3) se deseja parar o programa, vá no botão machine e selecione Stop

**Comandos:**

* Botão Compilar: Compila o programa Assembly
* Botão Iniciar: Inicia o programa compilado
* Botão Passo-a-Passo: Mostra passo a passo o programa
* Botão Limpar terminal: Limpa o terminal onde sai o programa
* Botão Resetar: Reinicia para iniciar o programa de novo
* Sistema/Parar: para o programa rodando
* Programas/Sair: encerra o programa
* Programas/Novo: começa um novo programa
* Programas/Carregar: carrega um programa salvo
* Programas/Salvar como... : salva o programa escrito agora
* Programas/Compilar: compila o programa escrito
* Programas/Carregar Arquivo adicional






* Comandos de ASSEMBLY para usar no viking

AND,OR,XOR  r1,r2,r3 = faz operação lógica com r2 e r3 e salva no r1
SLT (SET IF LESS THAN)
